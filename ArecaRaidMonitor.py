# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import json
import requests
from bs4 import BeautifulSoup
from requests.auth import HTTPDigestAuth
from sys import argv


def main():
    try:
        prtg = json.loads(argv[1])
        host = list(str.split(prtg['host']))[0]
        user = list(str.split(prtg['linuxloginusername']))[0]
        password = list(str.split(prtg['linuxloginpassword']))[0]
        url = "http://{}/".format(host)
        s = requests.session()
        s.headers.update({'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36"})
        s.auth = HTTPDigestAuth(user, password)

        return_val = {
            "prtg": {
                "result": [

                ]
            }
        }

        r = s.get(url)
        if r.status_code == 200:
            # get the website status
            return_val['prtg']['result'].append({
                        "channel": "Raid Controller Website",
                        "float": 0,
                        "value": 1,
                        "unit": "custom",
                        "customunit": "Status",
                        "limitmode": "1",
                        "limitminerror": "0"
                    })
            # get the status of raid vol 1
            r = s.get("http://{}/vol001.htm".format(host))
            if "Raid Set # 000" in r.text and "Normal" in r.text:
                return_val['prtg']['result'].append({
                    "channel": "Raid Status",
                    "float": 0,
                    "value": 1,
                    "customunit": "Status",
                    "limitmode": "1",
                    "limitminerror": "0"
                })
            else:
                return_val['prtg']['result'].append({
                    "channel": "Raid Status",
                    "float": 0,
                    "value": 0,
                    "limitmode": "1",
                    "limitminerror": "0"
                })

            # get CPU and controller temp and also CPU fan speed
            r = s.get("http://{}/hwmon.htm".format(host))
            soup = BeautifulSoup(r.text, 'html.parser')
            table = soup.find('table', attrs={"cellspacing": 1, "cellpadding": 2, "bgcolor": "B5B5B5"})
            for row in table.findAll('tr'):
                cells = row.findAll('td', {"class": "content"})
                if not len(cells) == 0:
                    if "CPU Temperature" in cells[0].get_text():
                        return_val['prtg']['result'].append({
                            "channel": cells[0].get_text(),
                            "float": 0,
                            "value": cells[1].get_text().replace(" ºC", ""),
                            "unit": "custom",
                            "customunit": "° Celsius",
                            "limitmode": "1",
                            "limitminerror": "0",
                            "limitmaxerror": "75"
                        })
                    elif "Controller Temp" in cells[0].get_text():
                        return_val['prtg']['result'].append({
                            "channel": cells[0].get_text(),
                            "float": 0,
                            "value": cells[1].get_text().replace(" ºC", ""),
                            "unit": "custom",
                            "customunit": "° Celsius",
                            "limitmode": "1",
                            "limitminerror": "0",
                            "limitmaxerror": "70"
                        })
                    elif "CPU Fan" in cells[0].get_text():
                        return_val['prtg']['result'].append({
                            "channel": cells[0].get_text(),
                            "float": 0,
                            "value": cells[1].get_text().replace(" RPM", ""),
                            "unit": "custom",
                            "limitmode": "1",
                            "customunit": "RPM",
                            "limitminerror": "0"
                        })
            # get the status for every HDD
            r = s.get("http://{}/hierarch.htm".format(host))
            soup = BeautifulSoup(r.text, 'html.parser')
            table = soup.find('table', attrs={"cellspacing": 1, "cellpadding": 2, "bgcolor": "B5B5B5"})
            for row in table.findAll('tr', {"bgcolor": "F0F0FF"}):
                cells = row.findAll('td', {"class": "linkcontent"})
                if not len(cells) == 0:
                    if "E#2Slot" in cells[0].get_text():
                        r = s.get("http://{}/{}".format(host, cells[0].find('a').get('href')))
                        soup = BeautifulSoup(r.text, 'html.parser')
                        table = soup.find('table', attrs={"cellspacing": 1, "cellpadding": 2, "bgcolor": "B5B5B5"})
                        cells = []
                        for row in table.findAll('tr', {"bgcolor": "FFFFDB"}):
                            cells.append(row.findAll('td', {"class": "content"}))
                        name = ""
                        status = 0
                        for cell in cells:
                            if "Device Location" in cell[0].get_text():
                                name = cell[1].get_text()
                            elif "Device Smart Status" in cell[0].get_text():
                                if "O.K." in cell[1].get_text():
                                    status = 1
                                else:
                                    status = 0
                            elif "Device State" in cell[0].get_text():
                                if "Normal" in cell[1].get_text():
                                    status = 1
                                else:
                                    status = 0
                        return_val['prtg']['result'].append({
                            "channel": name,
                            "float": 0,
                            "value": status,
                            "customunit": "Status",
                            "limitmode": "1",
                            "limitminerror": "0"
                        })
        return json.dumps(return_val)
    except Exception as e:
        return "{} \n {}".format(e, e.args)


if __name__ == '__main__':
    print(main())
